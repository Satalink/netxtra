/*
 * GUI.java
 *
 * Created on December 6, 2007, 4:53 PM
 */

package netxtra;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;


public class GUI extends javax.swing.JFrame {
    private Pkg_Interface iface;
    private Map<String, Color> colorMap;
    private Map<String, Font> fontMap;
    protected boolean alertsMapUpdating = false;
    public String operator = "nealg";
    public long alertSelectIdle = 0;
    public int alertUpdateDelay = 3;
    public int alertClearDelay = 15;
    public Map<Integer, Integer> alertsMap;
    protected ArrayList alertData;
    private JTable inhibitTable;
    private TableSorter sorter;
    public GUI(Pkg_Interface face) {
        iface = face;
        alertsMap = new HashMap<Integer, Integer>();
        fontMap = new HashMap<String, Font>();
            fontMap.put("GA10", new Font("Georgia", 0, 10));
            fontMap.put("GA10B", new Font("Georgia", 1, 10));
            fontMap.put("GA11", new Font("Georgia", 0, 11));
            fontMap.put("GA11B", new Font("Georgia", 1, 11));
            fontMap.put("GA12B", new Font("Georgia", 1, 12));
            fontMap.put("GA14", new Font("Georgia", 0, 14));
            fontMap.put("GA14B", new Font("Georgia", 1, 14));
            fontMap.put("GA16", new Font("Georgia", 0, 16));
            fontMap.put("GA16B", new Font("Georgia", 1, 16));
        colorMap = new HashMap<String, Color>();
            //GUI Basic Colors
            colorMap.put("black", new Color(0,0,0));
            colorMap.put("white", new Color(255,255,255));
            colorMap.put("header", new Color(200,200,200));
            colorMap.put("menu", new Color(220,220,220));
            
            //Gateway Colors
            colorMap.put("FAILED", new Color(237,66,43));
            colorMap.put("SEL_FAILED", new Color(199,55,36));
            colorMap.put("SUSPENDED", new Color(245,183,46));
            colorMap.put("SEL_SUSPENDED", new Color(207,154,39));                        
            colorMap.put("INACTIVE", new Color(230,222,41));
            colorMap.put("SEL_INACTIVE", new Color(191,184,34));            
            colorMap.put("ACTIVE", new Color(41,227,48));
            colorMap.put("SEL_ACTIVE", new Color(34,189,40));   
            colorMap.put("LOADING", new Color(251,93,240));
            colorMap.put("SEL_LOADING", new Color(212,78,203));  
            //Alert Colors
            colorMap.put("critical", new Color(255,80,80));
            colorMap.put("major", new Color(255,152,0));
            colorMap.put("minor", new Color(255,255,0));
            colorMap.put("clear", new Color(41,227,48));
            colorMap.put("normal", new Color(41,227,48));
            colorMap.put("indeterminate", new Color(251,93,240));
            colorMap.put("warning", new Color(46,192,255)); 
            //Inhibit Colors
            colorMap.put("inhTablePri", new Color(136,202,255));
            colorMap.put("inhTableAlt", new Color(212,228,255));
            colorMap.put("sel_inhTablePri", new Color(115,172,217));
            colorMap.put("sel_inhTableAlt", new Color(180,194,217));
        initComponents();
        initTabs();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent ex) {
                  iface.exitApp();
            }
        });
    }
   
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gateTablePopupMenu = new javax.swing.JPopupMenu();
        alertTablePopupMenu = new javax.swing.JPopupMenu();
        mainTab = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        alertdisplayScrollPane = new javax.swing.JScrollPane();
        alertTable = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        gatewayScrollPane = new javax.swing.JScrollPane();
        gateTableScrollPane = new javax.swing.JScrollPane();
        gateTable = new javax.swing.JTable();
        notifierTab = new javax.swing.JPanel();
        notifierInput = new javax.swing.JTextField();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jButton1 = new javax.swing.JButton();
        ruleviewerTab = new javax.swing.JTabbedPane();
        pautilTab = new javax.swing.JTabbedPane();
        svnTab = new javax.swing.JTabbedPane();
        statisticsTab = new javax.swing.JTabbedPane();
        processesTab = new javax.swing.JTabbedPane();
        tracesTab = new javax.swing.JTabbedPane();
        logsTab = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuItem_Exit = new javax.swing.JMenuItem();
        menuEditors = new javax.swing.JMenu();
        menuItem_AlertEditor = new javax.swing.JMenuItem();
        menuItem_AuthEditor = new javax.swing.JMenuItem();
        menuItem_DialogEditor = new javax.swing.JMenuItem();
        menuItem_GatewayEditor = new javax.swing.JMenuItem();
        menuItem_ObjectEditor = new javax.swing.JMenuItem();
        menuItem_RuleEditor = new javax.swing.JMenuItem();
        menuIDEAS = new javax.swing.JMenu();
        menuIDEASDump = new javax.swing.JMenu();
        menuItemIDEASDumpEvents = new javax.swing.JMenuItem();
        menuItemIDEASDumpEventSummary = new javax.swing.JMenuItem();
        menuIDEASReInit = new javax.swing.JMenu();
        menuItemIDEASReInitALL = new javax.swing.JMenuItem();
        menuItemIDEASReInitRules = new javax.swing.JMenuItem();
        menuItemIDEASReInitRulesOnly = new javax.swing.JMenuItem();
        menuItemIDEASReInitMib = new javax.swing.JMenuItem();
        menuItemIDEASReInitLOCK = new javax.swing.JMenuItem();
        menuItemIDEASReInitUNLOCK = new javax.swing.JMenuItem();
        menuIDEASStats = new javax.swing.JMenu();
        menuItemStatsALL = new javax.swing.JMenuItem();
        menuItemStatsALERTDEF = new javax.swing.JMenuItem();
        menuItemStatsATTR = new javax.swing.JMenuItem();
        menuItemStatsCACHELST = new javax.swing.JMenuItem();
        menuItemStatsCLASS = new javax.swing.JMenuItem();
        menuItemStatsCLASSATTR = new javax.swing.JMenuItem();
        menuItemStatsEVENT = new javax.swing.JMenuItem();
        menuItemStatsEVTSUM = new javax.swing.JMenuItem();
        menuItemStatsMEMSUM = new javax.swing.JMenuItem();
        menuItemStatsMEMTAB = new javax.swing.JMenuItem();
        menuItemStatsMO = new javax.swing.JMenuItem();
        menuItemStatsMOATTR = new javax.swing.JMenuItem();
        menuItemStatsMOREPAS = new javax.swing.JMenuItem();
        menuItemStatsRULE = new javax.swing.JMenuItem();
        menuItemStatsRULESTATS = new javax.swing.JMenuItem();
        menuIDEASTrace = new javax.swing.JMenu();
        menuItemIDEASTrace = new javax.swing.JMenuItem();
        menuOptions = new javax.swing.JMenu();
        menuOptionsALERTS = new javax.swing.JMenu();
        menuItemALERTSclear_CheckBox = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainTab.setDoubleBuffered(true);
        mainTab.setOpaque(true);

        alertTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "AMO", "Description", "Count", "Create Time", "Operator", "Class", "Alert Name", "Update Time", "Severity", "Alert"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        alertdisplayScrollPane.setViewportView(alertTable);

        jPanel2.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED)));

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 1198, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 63, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(10, 10, 10)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(10, 10, 10))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, alertdisplayScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1226, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(alertdisplayScrollPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE))
        );

        mainTab.addTab("Alerts", jPanel1);

        gateTable.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        gateTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Gateway Name", "Class", "Manager", "Host", "Port", "Status", "Archiving"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        gateTable.setColumnSelectionAllowed(true);
        gateTableScrollPane.setViewportView(gateTable);
        gateTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        gatewayScrollPane.setViewportView(gateTableScrollPane);

        mainTab.addTab("Gateways", gatewayScrollPane);

        notifierInput.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jScrollPane2.setViewportView(jTextPane1);

        jSplitPane1.setRightComponent(jScrollPane2);

        jButton1.setText("jButton1");

        org.jdesktop.layout.GroupLayout notifierTabLayout = new org.jdesktop.layout.GroupLayout(notifierTab);
        notifierTab.setLayout(notifierTabLayout);
        notifierTabLayout.setHorizontalGroup(
            notifierTabLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(notifierTabLayout.createSequentialGroup()
                .addContainerGap()
                .add(notifierTabLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSplitPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1206, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, notifierTabLayout.createSequentialGroup()
                        .add(notifierInput, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1123, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jButton1)))
                .addContainerGap())
        );
        notifierTabLayout.setVerticalGroup(
            notifierTabLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(notifierTabLayout.createSequentialGroup()
                .addContainerGap()
                .add(jSplitPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 608, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(notifierTabLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButton1)
                    .add(notifierInput, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(154, Short.MAX_VALUE))
        );

        mainTab.addTab("Notifier", notifierTab);
        mainTab.addTab("Event Viewer", ruleviewerTab);
        mainTab.addTab("NetExport", pautilTab);
        mainTab.addTab("SVN Tool", svnTab);
        mainTab.addTab("Statistics", statisticsTab);
        mainTab.addTab("Processes View", processesTab);
        mainTab.addTab("Trace Viewer", tracesTab);
        mainTab.addTab("Log Viewer", logsTab);

        menuFile.setText("File"); // NOI18N

        menuItem_Exit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        menuItem_Exit.setText("Exit"); // NOI18N
        menuItem_Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItem_ExitActionPerformed(evt);
            }
        });
        menuFile.add(menuItem_Exit);

        jMenuBar1.add(menuFile);

        menuEditors.setText("Editors"); // NOI18N

        menuItem_AlertEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        menuItem_AlertEditor.setText("Alert Editor"); // NOI18N
        menuEditors.add(menuItem_AlertEditor);

        menuItem_AuthEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        menuItem_AuthEditor.setText("Auth Editor"); // NOI18N
        menuEditors.add(menuItem_AuthEditor);

        menuItem_DialogEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        menuItem_DialogEditor.setText("Dialog Editor"); // NOI18N
        menuEditors.add(menuItem_DialogEditor);

        menuItem_GatewayEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        menuItem_GatewayEditor.setText("Gateway Editor"); // NOI18N
        menuEditors.add(menuItem_GatewayEditor);

        menuItem_ObjectEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        menuItem_ObjectEditor.setText("Object Editor"); // NOI18N
        menuEditors.add(menuItem_ObjectEditor);

        menuItem_RuleEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        menuItem_RuleEditor.setText("Rule Editor"); // NOI18N
        menuEditors.add(menuItem_RuleEditor);

        jMenuBar1.add(menuEditors);

        menuIDEAS.setText("Ideas"); // NOI18N

        menuIDEASDump.setText("Dump"); // NOI18N

        menuItemIDEASDumpEvents.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASDumpEvents.setText("Events"); // NOI18N
        menuIDEASDump.add(menuItemIDEASDumpEvents);

        menuItemIDEASDumpEventSummary.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASDumpEventSummary.setText("Event Summary"); // NOI18N
        menuIDEASDump.add(menuItemIDEASDumpEventSummary);

        menuIDEAS.add(menuIDEASDump);

        menuIDEASReInit.setText("ReInit"); // NOI18N

        menuItemIDEASReInitALL.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASReInitALL.setText("All"); // NOI18N
        menuIDEASReInit.add(menuItemIDEASReInitALL);

        menuItemIDEASReInitRules.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASReInitRules.setText("Rules"); // NOI18N
        menuIDEASReInit.add(menuItemIDEASReInitRules);

        menuItemIDEASReInitRulesOnly.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASReInitRulesOnly.setText("Rules Only"); // NOI18N
        menuIDEASReInit.add(menuItemIDEASReInitRulesOnly);

        menuItemIDEASReInitMib.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASReInitMib.setText("Mib"); // NOI18N
        menuIDEASReInit.add(menuItemIDEASReInitMib);

        menuItemIDEASReInitLOCK.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASReInitLOCK.setText("Lock"); // NOI18N
        menuIDEASReInit.add(menuItemIDEASReInitLOCK);

        menuItemIDEASReInitUNLOCK.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASReInitUNLOCK.setText("UnLock"); // NOI18N
        menuIDEASReInit.add(menuItemIDEASReInitUNLOCK);

        menuIDEAS.add(menuIDEASReInit);

        menuIDEASStats.setText("Stats"); // NOI18N

        menuItemStatsALL.setText("All"); // NOI18N
        menuIDEASStats.add(menuItemStatsALL);

        menuItemStatsALERTDEF.setText("Alert Definition"); // NOI18N
        menuIDEASStats.add(menuItemStatsALERTDEF);

        menuItemStatsATTR.setText("Attribute"); // NOI18N
        menuIDEASStats.add(menuItemStatsATTR);

        menuItemStatsCACHELST.setText("Cache List"); // NOI18N
        menuIDEASStats.add(menuItemStatsCACHELST);

        menuItemStatsCLASS.setText("Class"); // NOI18N
        menuIDEASStats.add(menuItemStatsCLASS);

        menuItemStatsCLASSATTR.setText("Class Attribute"); // NOI18N
        menuIDEASStats.add(menuItemStatsCLASSATTR);

        menuItemStatsEVENT.setText("Event"); // NOI18N
        menuIDEASStats.add(menuItemStatsEVENT);

        menuItemStatsEVTSUM.setText("Event Summary"); // NOI18N
        menuIDEASStats.add(menuItemStatsEVTSUM);

        menuItemStatsMEMSUM.setText("Memory Summary"); // NOI18N
        menuIDEASStats.add(menuItemStatsMEMSUM);

        menuItemStatsMEMTAB.setText("Memory Table"); // NOI18N
        menuIDEASStats.add(menuItemStatsMEMTAB);

        menuItemStatsMO.setText("Manage Object"); // NOI18N
        menuIDEASStats.add(menuItemStatsMO);

        menuItemStatsMOATTR.setText("MO Attribute"); // NOI18N
        menuIDEASStats.add(menuItemStatsMOATTR);

        menuItemStatsMOREPAS.setText("MO Reported As"); // NOI18N
        menuIDEASStats.add(menuItemStatsMOREPAS);

        menuItemStatsRULE.setText("Rules"); // NOI18N
        menuIDEASStats.add(menuItemStatsRULE);

        menuItemStatsRULESTATS.setText("Rule Stats"); // NOI18N
        menuIDEASStats.add(menuItemStatsRULESTATS);

        menuIDEAS.add(menuIDEASStats);

        menuIDEASTrace.setText("Trace"); // NOI18N

        menuItemIDEASTrace.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemIDEASTrace.setText("Trace"); // NOI18N
        menuIDEASTrace.add(menuItemIDEASTrace);

        menuIDEAS.add(menuIDEASTrace);

        jMenuBar1.add(menuIDEAS);

        menuOptions.setText("Options");

        menuOptionsALERTS.setText("Alerts");

        menuItemALERTSclear_CheckBox.setSelected(true);
        menuItemALERTSclear_CheckBox.setText("Clear Immediately");
        menuOptionsALERTS.add(menuItemALERTSclear_CheckBox);

        menuOptions.add(menuOptionsALERTS);

        jMenuBar1.add(menuOptions);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, mainTab, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1231, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, mainTab, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 835, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //Action Performs
    private void menuItem_ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItem_ExitActionPerformed
        iface.exitApp();
}//GEN-LAST:event_menuItem_ExitActionPerformed
    private void gateTablePopupMenuActionPerformed(ActionEvent event){
        final ipmhhook ghook = iface.getHook();
        if(ghook.ready) {
            if(event.getActionCommand().equals("Start")) {
                for(int i:gateTable.getSelectedRows()) {
                    int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !16^"+mgrPortKey+"@");
                }
            } else if (event.getActionCommand().equals("Stop")) {
                for(int i:gateTable.getSelectedRows()) {
                    int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !17^"+mgrPortKey+"^o@");
                }
            } else if (event.getActionCommand().equals("ReInit")) {
                for(int i:gateTable.getSelectedRows()) {
                    int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !98^"+mgrPortKey+"@");
                }
            } else if (event.getActionCommand().equals("Resume")) {
                for(int i:gateTable.getSelectedRows()) {
                    int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !103^"+mgrPortKey+"@");
                }
            } else if(event.getActionCommand().startsWith("Event Trace")) {
                for(int i:gateTable.getSelectedRows()) {
                    final int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    final int delaytime = Integer.valueOf(event.getActionCommand().split(" ")[2].split("\\(")[1]);
                    Runnable runnable = new Runnable() {
                        public void run() {
                                try {
                                    ghook.sendMessage("publish 500 !90^"+mgrPortKey+"@");
                                    Thread.sleep(60 * delaytime * 1000);
                                    ghook.sendMessage("publish 500 !91^"+mgrPortKey+"@");
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.setPriority(Thread.NORM_PRIORITY -1);
                    thread.start();
                }
            } else if(event.getActionCommand().startsWith("Event Watch")) {
                for(int i:gateTable.getSelectedRows()) {
                    final int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    final int delaytime = Integer.valueOf(event.getActionCommand().split(" ")[2].split("\\(")[1]);
                    Runnable runnable = new Runnable() {
                        public void run() {
                                try {
                                    ghook.sendMessage("publish 500 !58^"+mgrPortKey+"@");
                                    Thread.sleep(60 * delaytime * 1000);
                                    ghook.sendMessage("publish 500 !59^"+mgrPortKey+"@");
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.setPriority(Thread.NORM_PRIORITY -1);
                    thread.start();
                }            
            } else if(event.getActionCommand().startsWith("Event Dump")) {
                for(int i:gateTable.getSelectedRows()) {
                    final int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !99^"+mgrPortKey+"@");
                }            
            } else if(event.getActionCommand().startsWith("Dialog Trace")) {
                for(int i:gateTable.getSelectedRows()) {
                    final int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    final int delaytime = Integer.valueOf(event.getActionCommand().split(" ")[2].split("\\(")[1]);
                    Runnable runnable = new Runnable() {
                        public void run() {
                                try {
                                    ghook.sendMessage("publish 500 !88^"+mgrPortKey+"@");
                                    Thread.sleep(60 * delaytime * 1000);
                                    ghook.sendMessage("publish 500 !89^"+mgrPortKey+"@");
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.setPriority(Thread.NORM_PRIORITY -1);
                    thread.start();
                }            
            } else if(event.getActionCommand().startsWith("Dialog Watch")) {
                for(int i:gateTable.getSelectedRows()) {
                    final int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    final int delaytime = Integer.valueOf(event.getActionCommand().split(" ")[2].split("\\(")[1]);
                    Runnable runnable = new Runnable() {
                        public void run() {
                                try {
                                    ghook.sendMessage("publish 500 !56^"+mgrPortKey+"@");
                                    Thread.sleep(60 * delaytime * 1000);
                                    ghook.sendMessage("publish 500 !57^"+mgrPortKey+"@");
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.setPriority(Thread.NORM_PRIORITY -1);
                    thread.start();
                }
            } else if(event.getActionCommand().equals("Archiving On")) {
                for(int i:gateTable.getSelectedRows()) {
                    int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !19^"+mgrPortKey+"@");
                }
            } else if(event.getActionCommand().equals("Archiving Off")) {
                for(int i:gateTable.getSelectedRows()) {
                    int mgrPortKey = Integer.valueOf(gateTable.getModel().getValueAt(i, 9).toString().trim());
                    ghook.sendMessage("publish 500 !20^"+mgrPortKey+"@");
                }
            }
            gateTable.removeRowSelectionInterval(0, (gateTable.getRowCount() - 1));
        }
    }
    private void alertTablePopupMenuActionPerformed(ActionEvent event) {
        final ipmhhook ghook = iface.getHook();
        if(ghook.ready) {
            String sql = "select netxoper from netxoper where netxoper.name = \'"+operator+"\'";
            Map operID = (Map) iface.getOraDB().DbSelect(sql).get(0);
            String oid = operID.get("NETXOPER").toString();
            final Component tmpComp = (Component) event.getSource();
            if(event.getActionCommand().equals("Clear")) {
                String alerts = "";
                for(int i:alertTable.getSelectedRows()) {
                    int alertid = Integer.valueOf(alertTable.getModel().getValueAt(i, 0).toString().trim());
                    alerts += "~"+alertid;
                    if(alerts.length()>128){
                        ghook.sendMessage("publish 511 !12^"+oid+"~"+operator+alerts+"@");
                        alerts = "";
                    }
                }
                if(alerts.length() > 0) ghook.sendMessage("publish 511 !12^"+oid+"~"+operator+alerts+"@");
            } else if(event.getActionCommand().equals("Acknowledge")) {
                String alerts = "";
                for(int i:alertTable.getSelectedRows()) {
                    int alertid = Integer.valueOf(alertTable.getModel().getValueAt(i, 0).toString().trim());
                    alerts += "~"+alertid;
                    if(alerts.length()>128){
                        ghook.sendMessage("publish 511 !11^"+oid+"~"+operator+"~"+alerts+"@");
                        alerts = "";
                    }
                }
                if(alerts.length() > 0) ghook.sendMessage("publish 511 !11^"+oid+"~"+operator+"~"+alerts+"@");
            } else if(event.getActionCommand().equals("UnAcknowledge")) {
                String alerts = "";
                for(int i:alertTable.getSelectedRows()) {
                    int alertid = Integer.valueOf(alertTable.getModel().getValueAt(i, 0).toString().trim());
                    alerts += "~"+alertid;
                    if(alerts.length()>128){
                        ghook.sendMessage("publish 511 !28^UnAck~"+alerts+"@");
                        alerts = "";
                    }
                }
                if(alerts.length() > 0) ghook.sendMessage("publish 511 !28^UnAck~"+alerts+"@");            
            } else if(tmpComp.getName().startsWith("Inhibit")) {
                    final Runnable runnable = new Runnable() {
                        public void run() {
                            String sql = "";
                            JSlider tmpSlider = (JSlider) tmpComp.getParent().getComponent(4);
                            long starttimestamp = Calendar.getInstance().getTimeInMillis();
                            java.sql.Date startdate = new java.sql.Date(starttimestamp);
                            java.sql.Time starttime = new java.sql.Time(starttimestamp);
                            String starts = startdate+":"+starttime;
                            long endtimestamp = (starttimestamp + (tmpSlider.getValue() * 3600000));
                            java.sql.Date enddate = new java.sql.Date(endtimestamp);
                            java.sql.Time endtime = new java.sql.Time(endtimestamp);
                            String ends = enddate+":"+endtime;
                            Map inhMOs = new HashMap();
                            iface.getHook().sendMessage("publish 500 '!72^"+operator+"^Inhibiting "+alertTable.getSelectedRows().length+" Alerts@'");
                            iface.getHook().sendMessage("publish 500 !73@");
                            for (int i:alertTable.getSelectedRows()) {
                                int alertid = 2147483647;
                                String amo = alertTable.getModel().getValueAt(i, 2).toString().trim();
                                if(!tmpComp.getName().equals("Inhibit AMO ALL")) {
                                    String alertname = alertTable.getModel().getValueAt(i, 8).toString().trim();
                                    sql = "select alertid from alertid where alertid.name = \'"+alertname+"\'";
                                    Map alertMap = (Map) iface.getOraDB().DbSelect(sql).get(0);
                                    alertid = Integer.valueOf(alertMap.get("ALERTID").toString().trim());
                                }
                                sql = "select mo from mo where mo.name = \'"+amo+"\'";
                                Map amoMap = (Map) iface.getOraDB().DbSelect(sql).get(0);
                                int amoid = Integer.valueOf(amoMap.get("MO").toString().trim());
                                if(!tmpComp.getName().equals("Inhibit AMO ALL")||inhMOs.get(amo) == null) {
                                    sql = 
                                            "insert into inhibit "+
                                            "VALUES("+
                                            amoid+","+
                                            alertid+","+
                                            "1,"+
                                            "TO_DATE('"+starts+"','YYYY-MM-DD:HH24:MI:SS'),"+
                                            "TO_DATE('"+ends+"','YYYY-MM-DD:HH24:MI:SS')"+
                                            ")";
                                    iface.getOraDB().DbStatement(sql);
                                    inhMOs.put(amo, amoid);
                                }
                                iface.getHook().sendMessage("publish 511 !8^inhibit~"+inhMOs.get(amo)+"@");
                                alertTable.removeRowSelectionInterval(i, i);
                            }
                            sql = "COMMIT";
                            iface.getOraDB().DbStatement(sql);
                        }
                    };
                    Thread thread = new Thread(runnable);
                    thread.setPriority(Thread.NORM_PRIORITY -1);
                    thread.start();                                
            } else if (tmpComp.getName().equals("Delete Inhibit Items")) {
                for(int i:inhibitTable.getSelectedRows()) {
                    String moName = inhibitTable.getModel().getValueAt(i, 0).toString().trim();
                    sql = "select mo from mo where mo.name = \'"+moName+"\'";
                    Map amoMap = (Map) iface.getOraDB().DbSelect(sql).get(0);
                    int amoid = Integer.valueOf(amoMap.get("MO").toString().trim());                
                    String alertName = inhibitTable.getModel().getValueAt(i, 1).toString().trim();
                    if (!alertName.equals("<ALL>")) {
                      sql = "delete from inhibit where inhibit.mo = "+amoid+" "+
                            "and inhibit.id in "+
                            "(select alertid.alertid from alertid where alertid.name = '"+alertName+"')";
                    } else {
                        sql = "delete from inhibit where inhibit.mo = "+amoid+" "+
                              "and inhibit.id = 2147483647";
                    }
                    iface.getOraDB().DbStatement(sql);
                    iface.getHook().sendMessage("publish 511 !8^inhibit~"+amoid+"@");
                }
                sql = "COMMIT";
                iface.getOraDB().DbStatement(sql);
                createALMTablePopupMenu();
            }
            if(tmpComp.getName() != null) {
                if(!tmpComp.getName().startsWith("Inhibit")) {
                    alertTable.removeRowSelectionInterval(0, (alertTable.getRowCount() - 1));
                }
            } else {
                alertTable.removeRowSelectionInterval(0, (alertTable.getRowCount() - 1));
            }
        }
    }
    private void alertReassignListActionPerformed(String username) {
        String sql = "select netxoper from netxoper where netxoper.name = \'"+username+"\'";
        Map operID = (Map) iface.getOraDB().DbSelect(sql).get(0);
        String roid = operID.get("NETXOPER").toString();
        String alerts = "";
        for(int i:alertTable.getSelectedRows()) {
            int alertid = Integer.valueOf(alertTable.getModel().getValueAt(i, 0).toString().trim());
            alerts += "~"+alertid;
        }
        if(iface.getHook().ready) iface.getHook().sendMessage("publish 511 !29^"+roid+"~"+username+alerts+"@");
    }

    // Change States
    private void gateMenuSliderStateChanged(ChangeEvent evt) {
        JSlider tmpslider = (JSlider) evt.getSource();
        int menuindex = 0;
        if (tmpslider.getName().equals("Event")) {
            menuindex = 5;
        } else if (tmpslider.getName().equals("Dialog")) {
            menuindex = 6;
        }
        int delaytime = tmpslider.getValue();
        String interval = null;
        if(delaytime < 2) {
            interval = "Minute";
        } else {
            interval = "Minutes";
        }
        JMenu tmpMenu = (JMenu) gateTablePopupMenu.getComponent(menuindex);
        Component[] menuComponents = (Component[]) tmpMenu.getMenuComponents();
        for (Component tmpComponent: menuComponents) {
            if(tmpComponent.toString().contains("JMenuItem")) {
                JMenuItem tmpMenuItem = (JMenuItem) tmpComponent;
                if(!tmpMenuItem.getName().contains("Dump")) {
                    String menuItemName = tmpMenuItem.getName();
                    tmpMenuItem.setText(menuItemName+" ("+delaytime+" "+interval+")");                    
                }
            }
        }
    }
    private void alertMenuSliderStateChanged(ChangeEvent evt) {
        JSlider tmpslider = (JSlider) evt.getSource();
        int menuindex = 0;
        
        if (tmpslider.getName().equals("Inhibit")) {
            menuindex = 5;
        }
        int delaytime = tmpslider.getValue();
        String interval = null;
        if(delaytime < 2) {
            interval = "Hour";
        } else {
            interval = "Hours";
        }
        JMenu tmpMenu = (JMenu) alertTablePopupMenu.getComponent(menuindex);
        JMenuItem tmpMenuItem = (JMenuItem) tmpMenu.getMenuComponent(3);
        tmpMenuItem.setEnabled(true);
        tmpMenuItem.setText("Inhibit for "+delaytime+" "+interval);                    
        tmpMenuItem.setEnabled(false);
    }

    // Trees
    public void createjTreeModel(String[] nodeArray) {
        //Create Root Node
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("NetExpert");
        DefaultMutableTreeNode node;
        Map<String, DefaultMutableTreeNode> nodeMap = new HashMap<String, DefaultMutableTreeNode>();
        for (int i=0;i < nodeArray.length;i++) {
            DefaultMutableTreeNode parent = root;
            String[] nodenames = nodeArray[i].toString().split("\\.");
            String nodeString = "";
            for (String nodename : nodenames) {
                nodeString += nodename;
                if(nodeMap.get(nodeString) != null) {
                    parent = nodeMap.get(nodeString);
                } else {
                    node = createjTreeNode(parent, nodename);
                    nodeMap.put(nodeString, node);                  
                }
            }
        }
        DefaultTreeModel treeModel = new DefaultTreeModel(root);
//        cmdTree.setModel(treeModel);
    }
    public DefaultMutableTreeNode createjTreeNode(DefaultMutableTreeNode parentnode, String nodename) {
        DefaultMutableTreeNode parent = new DefaultMutableTreeNode(nodename);
        parentnode.add(parent);
        return(parent);
    }

    // Sliders
    public JSlider createGateMenuSlider(String name, int[] values) {
        JSlider slide = new JSlider();
        slide.setName(name);
        slide.setOrientation(JSlider.HORIZONTAL);
        slide.setSnapToTicks(true);
        slide.setBackground(colorMap.get("menu"));
        slide.setForeground(colorMap.get("black"));
        slide.setMinimum(values[0]);
        slide.setMaximum(values[1]);
        slide.setMinorTickSpacing(values[2]);
        slide.setMajorTickSpacing(values[3]);
        slide.setPaintLabels(true);
        slide.setPaintTicks(true);
        slide.setValue(1);
        slide.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                gateMenuSliderStateChanged(evt);
            }
        });
        slide.setToolTipText("Duration in Minutes");
        return(slide);
    }
    public JSlider createALMMenuSlider(String name, int[] values) {
        JSlider slide = new JSlider();
        slide.setName(name);
        slide.setOrientation(JSlider.HORIZONTAL);
        slide.setSnapToTicks(true);
        slide.setBackground(colorMap.get("menu"));
        slide.setForeground(colorMap.get("black"));
        slide.setMinimum(values[0]);
        slide.setMaximum(values[1]);
        slide.setMinorTickSpacing(values[2]);
        slide.setMajorTickSpacing(values[3]);
        slide.setPaintLabels(true);
        slide.setPaintTicks(true);
        slide.setValue(1);
        slide.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                alertMenuSliderStateChanged(evt);
            }
        });
        slide.setToolTipText("Duration in Hours");
        return(slide);
    }

    // Popup Menus
    public void createALMTablePopupMenu() {
        alertTablePopupMenu = new JPopupMenu();
        ArrayList<JMenuItem> tableMenuItems = new ArrayList<JMenuItem>();
        tableMenuItems.add(new JMenuItem("Clear"));
        tableMenuItems.add(new JMenuItem("Acknowledge"));
        tableMenuItems.add(new JMenuItem("UnAcknowledge"));
        for (JMenuItem menuitem : tableMenuItems) {
            menuitem.setBackground(colorMap.get("menu"));
            menuitem.setForeground(colorMap.get("black"));
            alertTablePopupMenu.add(menuitem);
        }
        String sql = "select name from netxoper where netxoper > 0 order by name";
        ArrayList operList = iface.getOraDB().DbSelect(sql);
        DefaultListModel usrlistModel = new DefaultListModel();
        int operscount = operList.size();
        for(int i=0;i<operscount;i++) {
            Map usrnameMap = (Map) operList.get(i);
            usrlistModel.addElement(usrnameMap.get("NAME"));
        }
        final JList usrlist = new JList(usrlistModel);
        usrlist.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        usrlist.setLayoutOrientation(JList.VERTICAL_WRAP);
        int listcols = 1;
        if (operscount > 2) listcols = (operscount / 3);
        usrlist.setVisibleRowCount(listcols);
        JScrollPane usrlistScrollPane = new JScrollPane(usrlist);
        usrlistScrollPane.setPreferredSize(new Dimension(250,200));
        JMenu reassign = new JMenu("Reassign");
        MouseListener userlistMouseListener = new MouseAdapter() {
	    @Override
		public void mousePressed(MouseEvent mc) {
            JList usrMClist = (JList) usrlist.getComponentAt(mc.getX(), mc.getY());
            String reassignUser = (String) usrMClist.getSelectedValue();
            if(mc.getClickCount() == 2) {
                alertReassignListActionPerformed(reassignUser);
                alertTablePopupMenu.setVisible(false);
            }
        }};
        usrlist.addMouseListener(userlistMouseListener);
        reassign.setName("Reassign");
        reassign.setForeground(colorMap.get("black"));
        reassign.setBackground(colorMap.get("menu"));
        reassign.add(usrlistScrollPane);
        alertTablePopupMenu.add(reassign);
        alertTablePopupMenu.addSeparator();
        int[] slidevals = {0, 24, 1, 6};        
        JMenu inhibit = new JMenu("Inhibit");
        inhibit.setName("Inhibit");
        inhibit.setForeground(colorMap.get("black"));
        inhibit.setBackground(colorMap.get("menu"));       
        JMenuItem inhAMOAll = new JMenuItem("AMO  <ALL>");
        inhAMOAll.setName("Inhibit AMO ALL");
        tableMenuItems.add(inhAMOAll);
        JMenuItem inhAMOAlm = new JMenuItem("AMO  <AlertName>");
        inhAMOAlm.setName("Inhibit AMO AlertName");
        tableMenuItems.add(inhAMOAlm);  
        inhibit.add(inhAMOAll);
        inhibit.add(inhAMOAlm);
        inhibit.addSeparator();
        JSlider inhibitslide = createALMMenuSlider("Inhibit", slidevals);
        JMenuItem delayLabel = new JMenuItem("Inhibit for "+inhibitslide.getValue()+" Hour");
        delayLabel.setEnabled(false);
        delayLabel.setName("DelayLabel");
        delayLabel.setBackground(colorMap.get("menu"));
        delayLabel.setForeground(colorMap.get("black"));
        delayLabel.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        delayLabel.setHorizontalAlignment(JLabel.CENTER);
        inhibit.add(delayLabel);
        inhibit.add(inhibitslide);
        alertTablePopupMenu.add(inhibit);
        JMenu uninhibit = new JMenu("Un-Inhibit");
        uninhibit.setBackground(colorMap.get("menu"));
        uninhibit.setForeground(colorMap.get("black"));    
        JScrollPane inhibitScrollPane = new JScrollPane();
        inhibitTable = createINHTableModel();
        inhibitTable.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        inhibitScrollPane.setViewportView(inhibitTable);
        uninhibit.add(inhibitScrollPane);
        alertTablePopupMenu.add(uninhibit);
        JMenuItem inhTableDeleteItems = new JMenuItem("Delete Items");
        inhTableDeleteItems.setName("Delete Inhibit Items");
        uninhibit.add(inhTableDeleteItems);
        tableMenuItems.add(inhTableDeleteItems);
        alertTablePopupMenu.setBackground(colorMap.get("menu"));
        alertTablePopupMenu.setForeground(colorMap.get("black"));
        alertTablePopupMenu.setBorder(new SoftBevelBorder(BevelBorder.LOWERED));
        alertTablePopupMenu.setFont(fontMap.get("GA10"));
        alertTable.add(alertTablePopupMenu);	
        alertTable.setComponentPopupMenu(alertTablePopupMenu);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
        for (JMenuItem menuitem: tableMenuItems) {
            menuitem.setBackground(colorMap.get("menu"));
            menuitem.setForeground(colorMap.get("balck"));
            menuitem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        alertTablePopupMenuActionPerformed(event);
                    }
                }
            );
        }
    }
    public void createGWTablePopupMenu() {
        gateTablePopupMenu = new JPopupMenu();
        ArrayList<JMenuItem> tableMenuItems = new ArrayList<JMenuItem>();
        tableMenuItems.add(new JMenuItem("Start"));
        tableMenuItems.add(new JMenuItem("Stop"));
        tableMenuItems.add(new JMenuItem("ReInit"));
        tableMenuItems.add(new JMenuItem("Resume"));
        for (JMenuItem menuitem : tableMenuItems) {
        //        for (int i=0;i < tableMenuItems.length;i++){
            menuitem.setBackground(colorMap.get("menu"));
            menuitem.setForeground(colorMap.get("black"));
            gateTablePopupMenu.add(menuitem);
        }
        // Action and mouse listener support
        gateTablePopupMenu.addSeparator();
        int[] slidevals = {0, 60, 1, 5};        
        JMenu events = new JMenu("Events");
        events.setName("Events");
        events.setForeground(colorMap.get("black"));
        events.setBackground(colorMap.get("menu"));
        JMenuItem evtTrace = new JMenuItem("Event Trace (1 Minute)");
        evtTrace.setName("Event Trace");
        evtTrace.setBackground(colorMap.get("menu"));
        evtTrace.setForeground(colorMap.get("black"));
        tableMenuItems.add(evtTrace);
        JMenuItem evtWatch = new JMenuItem("Event Watch (1 Minute)");
        evtWatch.setName("Event Watch");
        evtWatch.setBackground(colorMap.get("menu"));
        evtWatch.setForeground(colorMap.get("black"));
        tableMenuItems.add(evtWatch);
        JMenuItem evtDump = new JMenuItem("Event Dump");
        evtDump.setName("Event Dump");
        evtDump.setBackground(colorMap.get("menu"));
        evtDump.setForeground(colorMap.get("black"));
        tableMenuItems.add(evtDump);
        events.add(evtDump);
        events.insertSeparator(1);
        events.add(evtTrace);
        events.add(evtWatch);
        JSlider eventslide = createGateMenuSlider("Event", slidevals);
        events.add(eventslide);
        JMenu dialogs = new JMenu("Dialogs");
        dialogs.setName("Dialogs");
        dialogs.setForeground(colorMap.get("black"));
        dialogs.setBackground(colorMap.get("menu"));
        JMenuItem diaTrace = new JMenuItem("Dialog Trace (1 Minute)");
        diaTrace.setName("Dialog Trace");
        diaTrace.setBackground(colorMap.get("menu"));
        diaTrace.setForeground(colorMap.get("black"));
        tableMenuItems.add(diaTrace);
        JMenuItem diaWatch = new JMenuItem("Dialog Watch (1 Minute)");
        diaWatch.setName("Dialog Watch");
        diaWatch.setBackground(colorMap.get("menu"));
        diaWatch.setForeground(colorMap.get("black"));
        tableMenuItems.add(diaWatch);
        dialogs.add(diaTrace);
        dialogs.add(diaWatch);
        JSlider diaslide = createGateMenuSlider("Dialog", slidevals);
        dialogs.add(diaslide);
        JMenu archive = new JMenu("Archiving");
        archive.setName("Archiving");
        archive.setForeground(colorMap.get("black"));
        archive.setBackground(colorMap.get("menu"));        
        JMenuItem arcOn = new JMenuItem("Archiving On");
        arcOn.setBackground(colorMap.get("menu"));
        arcOn.setForeground(colorMap.get("black"));
        tableMenuItems.add(arcOn);
        JMenuItem arcOff = new JMenuItem("Archiving Off");
        arcOff.setBackground(colorMap.get("menu"));
        arcOff.setForeground(colorMap.get("black"));
        tableMenuItems.add(arcOff);
        archive.add(arcOn);
        archive.add(arcOff);
        gateTablePopupMenu.add(events);
        gateTablePopupMenu.add(dialogs);
        gateTablePopupMenu.add(archive);
        gateTablePopupMenu.setBackground(colorMap.get("menu"));
        gateTablePopupMenu.setForeground(colorMap.get("black"));
        gateTablePopupMenu.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        gateTablePopupMenu.setFont(fontMap.get("GA10"));
        gateTable.add(gateTablePopupMenu);	
        gateTable.setComponentPopupMenu(gateTablePopupMenu);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
        for (JMenuItem menuitem: tableMenuItems) {
            menuitem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        gateTablePopupMenuActionPerformed(event);
                    }
                }
            );
        }
    }

    // Tables
    public void createAlertTableModel(ArrayList tableData) {
        DefaultTableModel model = new DefaultTableModel(new String[]{"Alert","Severity","AMO","Description","Count","Update Time","Operator","Class","Alert Name","Create Time"}, 0) {
            @Override
            public Class getColumnClass(int columnIndex) {
                return columnIndex==0? String.class : String.class;
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex){
                return false;
            }           
        };
        sorter = new TableSorter(model);
        alertTable.setName("Alert");
        alertTable.setModel(sorter);
        ToolTipManager.sharedInstance().setEnabled(false);
        CustomAlertDataRenderer custom = new CustomAlertDataRenderer();
        alertTable.getTableHeader().setDefaultRenderer(new CustomHeaderRenderer());
        sorter.setTableHeader(alertTable.getTableHeader());
        alertTable.setTableHeader(sorter.getTableHeader());
        int[] colsizes = new int[]{0,0,100,300,25,80,40,40,40,40,40};
        for (int i=0; i < alertTable.getModel().getColumnCount();i++) {
           alertTable.getColumnModel().getColumn(i).setCellRenderer(custom);
           alertTable.getColumnModel().getColumn(i).setPreferredWidth(colsizes[i]);
           
           if(i==0||i==1){
                alertTable.getColumnModel().getColumn(i).setMinWidth(colsizes[i]);
                alertTable.getColumnModel().getColumn(i).setMaxWidth(colsizes[i]);
           }
        }
        alertTable.setRowSelectionAllowed(true);
        alertTable.setColumnSelectionAllowed(false);
        if (tableData == null) return;
        alertsMap.clear();
        for(int i=0;i<tableData.size();i++) {
            Map alertMap = (Map) tableData.get(i);
            // Substitute Operator UnAssigned with whitespace.
            if(alertMap.get("Operator").equals("Unassigned")) {
                alertMap.put("Operator", " ");
            }
            model.addRow(new Object[]{
                new String(""+alertMap.get("Alert")),
                new String(""+alertMap.get("Severity")),
                new String(""+alertMap.get("AMO")),
                new String(""+alertMap.get("Description")),
                new String(""+alertMap.get("Count")),
                new String(""+alertMap.get("UpdateTime")),
                new String(""+alertMap.get("Operator")),
                new String(""+alertMap.get("Class")),
                new String(""+alertMap.get("AlertName")),
                new String(""+alertMap.get("CreateTime"))
            });
            alertsMap.put(Integer.valueOf(alertMap.get("Alert").toString().trim()), i);
        }
        alertData = tableData;
        MouseListener alertTableMouseListener = new MouseAdapter() {
	    @Override
		public void mousePressed(MouseEvent mc) {
                Point coords = mc.getPoint();
                if(mc.getButton() == MouseEvent.BUTTON3 && alertTable.getSelectedRowCount() < 2) {
                    int rowNumber = alertTable.rowAtPoint(coords);
                    alertTable.getSelectionModel().setSelectionInterval(rowNumber, rowNumber);
                }
                if(mc.getButton() == MouseEvent.BUTTON2) {
                    ToolTipManager.sharedInstance().setEnabled(true);
                    ToolTipManager.sharedInstance().setInitialDelay(5);
                } else {
                    ToolTipManager.sharedInstance().setEnabled(false);
                }
        }};
        alertTable.addMouseListener(alertTableMouseListener);
        createALMTablePopupMenu();
    }
    public void createGWtableModel(ArrayList tableData) {
       DefaultTableModel model = new DefaultTableModel(new String[]{"Status","Gateway Name","Class Name","Manager Name","Host","Port","Dia","Cut","Arc","GWPortKey"}, 0) {
            @Override
            public Class getColumnClass(int columnIndex) {
                return columnIndex==0? Icon.class : String.class;
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex){
                return false;
            }           
        };
       CustomGatewayDataRenderer custom = new CustomGatewayDataRenderer();
       CustomHeaderRenderer custheader = new CustomHeaderRenderer();
       //TODO Build Table Data
       if(tableData == null) return;
       for(int i =0;i<tableData.size();i++) {
            Map gateMap = (Map) tableData.get(i);
            if (gateMap.size() == 5) {
                model.addRow(new Object[]{
                    new String(" "),
                    new String(""+gateMap.get("GatewayName")),
                    new String(""+gateMap.get("ClassName")),
                    new String(""+gateMap.get("ManagerName")),
                    new String(""+gateMap.get("HostName")),
                    new String(""+gateMap.get("PortName")),
                    new String(" "),
                    new String(" "),
                    new String(" "),
                    new String(" ")
                });	
            } else {
                model.addRow(new Object[]{
                    new String(""+gateMap.get("Status")),
                    new String(""+gateMap.get("GatewayName")),
                    new String(""+gateMap.get("ClassName")),
                    new String(""+gateMap.get("ManagerName")),
                    new String(""+gateMap.get("HostName")),
                    new String(""+gateMap.get("PortName")),
                    new String(""+gateMap.get("Dialog")),
                    new String(""+gateMap.get("CutThrough")),
                    new String(""+gateMap.get("Archive")),
                    new String(""+gateMap.get("MgrPortKey"))
                });	            
            }
       }
       gateTable.getTableHeader().setDefaultRenderer(custheader);
       gateTable.setModel(model);
       gateTable.setName("Gateway");
       int[] colsizes = new int[]{0,220,140,140,140,80,40,40,40,0};
       for (int i=0; i < gateTable.getModel().getColumnCount();i++) {
           gateTable.getColumnModel().getColumn(i).setCellRenderer(custom);
           gateTable.getColumnModel().getColumn(i).setPreferredWidth(colsizes[i]);
           if(i==0||i==9){
                gateTable.getColumnModel().getColumn(i).setMinWidth(colsizes[i]);
                gateTable.getColumnModel().getColumn(i).setMaxWidth(colsizes[i]);
           } else if (i==6||i==7|i==8) {
               gateTable.getColumnModel().getColumn(i).setMinWidth(colsizes[i]);
           } else if (i==4) {
               gateTable.getColumnModel().getColumn(i).setMinWidth(80);
           } else {
               gateTable.getColumnModel().getColumn(i).setMinWidth(140);
           }
       }
       gateTable.setRowSelectionAllowed(true);
       gateTable.setColumnSelectionAllowed(false);

       createGWTablePopupMenu();
    }
    public JTable createINHTableModel() {
        JTable inhTable = new JTable();
        inhTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String [] {
                "AMO", "Alert", "Started", "Expires"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        //Delete Old Inhibits from table;
        String sql = 
                "delete from inhibit "+
                "where ENDDATE < SYSDATE";
                iface.getOraDB().DbStatement(sql);  
                sql = "commit";
                iface.getOraDB().DbStatement(sql);
        //Select Existing Inibits
        sql =
                "select "+
                "mo.name as \"AMO\", "+
                "alertid.name as \"AlertName\", "+
                "to_char (inhibit.startdate, 'YYYY-MM-DD  HH24:MI') as \"Started\", "+
                "to_char (inhibit.enddate, 'YYYY-MM-DD  HH24:MI') as \"Expires\" "+
                "from inhibit "+
                "left join alertid on alertid.alertid = inhibit.id "+
                "left join mo on mo.mo = inhibit.mo "+
                "order by inhibit.enddate";
        ArrayList inhibitData = iface.getOraDB().DbSelect(sql);
        DefaultTableModel model = new DefaultTableModel(new String[]{"AMO","AlertName","Started","Expires"}, 0) {
            @Override
            public Class getColumnClass(int columnIndex) {
                return columnIndex==0? String.class : String.class;
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex){
                return false;
            }           
        };        
        CustomInhibitDataRenderer custom = new CustomInhibitDataRenderer();
        CustomHeaderRenderer custheader = new CustomHeaderRenderer();
        if (inhibitData != null) {
//          alertsMap.clear();
            for(int i=0;i<inhibitData.size();i++) {
                Map inhibitMap = (Map) inhibitData.get(i);
                String alertName;
                if(inhibitMap.get("AlertName") == null) {
                    alertName = "<ALL>";
                } else {
                    alertName = (String) inhibitMap.get("AlertName");
                }
                model.addRow(new Object[]{
                    new String(""+inhibitMap.get("AMO")),
                    new String(""+alertName),
                    new String(""+inhibitMap.get("Started")),
                    new String(""+inhibitMap.get("Expires"))
                });
            }
           inhTable.getTableHeader().setDefaultRenderer(custheader);
           inhTable.setModel(model);
           inhTable.setName("Inhibit");
           int[] colsizes = new int[]{100,100,90,90};
           for (int i=0; i < inhTable.getModel().getColumnCount();i++) {
               inhTable.getColumnModel().getColumn(i).setCellRenderer(custom);
               inhTable.getColumnModel().getColumn(i).setPreferredWidth(colsizes[i]);
           }
           inhTable.setRowSelectionAllowed(true);
           inhTable.setColumnSelectionAllowed(false);  
        }
        return(inhTable);
    }
    public void removeALMtableItem(final int alertid) {
        int row = alertsMap.get(alertid);
        DefaultTableModel model = (DefaultTableModel) sorter.getTableModel();
        if (menuItemALERTSclear_CheckBox.isSelected()) {
            model.removeRow(row);
        } else {
            alertTable.getModel().setValueAt(new String("normal"), row, 1);
            alertTable.repaint();
            Thread thread = new Thread("ClearThread") {
                @Override
                @SuppressWarnings("static-access")
                public void run() {
                boolean status=true;
                int delay = alertClearDelay;
                    while(status){
                        try {
                            int miliBuffer = iface.genRandom(1000);
                            Thread.currentThread().sleep((delay * 1000)+miliBuffer);
                            int row = alertsMap.get(alertid);
                            DefaultTableModel model = (DefaultTableModel) sorter.getTableModel();
                            if(alertid == Integer.valueOf(model.getValueAt(row, 0).toString().trim())) {
                                model.removeRow(row);
                                status=false;
                            }
                        } catch (InterruptedException ex) {
                            delay = 0;
                        }
                    }
                }
            };
            thread.start();
        }
        mainTab.setTitleAt(0, "Alerts ("+alertTable.getRowCount()+")");
        updateAlertsMap();
    }
    public void updateAlertTableModel(Map alertMap, final int almIDX) {
        DefaultTableModel model = (DefaultTableModel) sorter.getTableModel();
        if (model == null || alertMap == null) return; 
        if(almIDX == -1){
            final Object[] almObj = new Object[]{
                    new String(alertMap.get("Alert").toString()),
                    new String(alertMap.get("Severity").toString()),
                    new String(alertMap.get("AMO").toString()),
                    new String(alertMap.get("Description").toString()),
                    new String(alertMap.get("Count").toString()),
                    new String(alertMap.get("UpdateTime").toString()),
                    new String(alertMap.get("Operator").toString()),
                    new String(alertMap.get("Class").toString()),
                    new String(alertMap.get("AlertName").toString()),
                    new String(alertMap.get("CreateTime").toString())              
            };
            model.addRow(almObj);
        } else {
            for(int col=0;col<alertMap.size();col++) {
                String colName = model.getColumnName(col).replace(" ", "");
                String ndat = alertMap.get(colName).toString();
                String odat = model.getValueAt(almIDX, col).toString();
                if(!odat.equals(ndat)) {
                    model.setValueAt(new String(ndat), almIDX, col);
                    model.fireTableDataChanged();
                }
            }
        }
        mainTab.setTitleAt(0, "Alerts ("+alertTable.getRowCount()+")");
        updateAlertsMap();
    }
    // Misc
    public void enableAlertTable(boolean b){
        alertTable.setEnabled(b);
    }
    public boolean isAlertTableHighLighted() {
        boolean bool = false;
        long now = Calendar.getInstance().getTimeInMillis()/1000;
        if(alertTable.getSelectedRowCount() > 0) {
            bool = true;
            if(alertSelectIdle == 0) {
                alertSelectIdle = now;
            } else if((alertSelectIdle+30)<now){
                alertSelectIdle = 0;
                alertTable.removeRowSelectionInterval(0, alertTable.getRowCount() - 1);
                bool = false;
            }
        }
        return(bool);
    }
    public boolean isAlertTablePopupMenuActive() {
        return(alertTablePopupMenu.isVisible());
    }
    public void updateAlertsMap() {
        alertsMapUpdating = true;
        alertsMap.clear();
        DefaultTableModel model = (DefaultTableModel) sorter.getTableModel();
        Vector alertVectors = model.getDataVector();
        for(int i=0;i<alertVectors.size();i++) {
            Vector alertVector = (Vector) alertVectors.elementAt(i);
            int alertid = Integer.valueOf(alertVector.get(0).toString().trim());
            alertsMap.put(alertid, i);
        }
        alertsMapUpdating = false;
    }
    public void updateALMtable() {
        ArrayList almData = iface.getAlertData();
        // Scan alertlog results for updates
        for (int i=0;i<almData.size();i++) {
            // AlertMap from DB ArrayList
            Map alertMap = (Map) almData.get(i);
            int alertid = Integer.valueOf(alertMap.get("Alert").toString());
            Map tableMap = new HashMap();
            int almIDX = alertsMap.get(alertid) == null ? -1 : alertsMap.get(alertid);
            for(int col=0;col<alertMap.size();col++) {
                String colName = sorter.getTableModel().getColumnName(col).toString().trim().replace(" ", "");
                if(alertMap.get(colName).equals(null) || alertMap.get(colName).equals("Unassigned")) {
                    alertMap.put(colName, " ");
                }
                if (almIDX > -1) tableMap.put(colName, sorter.getTableModel().getValueAt(i, col));
            }
            if(!alertMap.equals(tableMap)) {
                updateAlertTableModel(alertMap, almIDX);
            }
        }
    }
    public void updateALMtable(int alertid) {
        ArrayList almData = iface.getAlertData(alertid);
        // Scan alertlog results for updates
        for (int i=0;i<almData.size();i++) {
            // AlertMap from DB ArrayList
            Map alertMap = (Map) almData.get(i);
            Map tableMap = new HashMap();
            int almIDX = alertsMap.get(alertid) == null ? -1 : alertsMap.get(alertid);
            if(almIDX > -1) tableMap = (Map) alertData.get(almIDX);
            if(alertMap.get("Operator").equals("Unassigned")) alertMap.put("Operator", " ");
            if(!alertMap.equals(tableMap)) {
                updateAlertTableModel(alertMap, almIDX);
            }
        }        
    }
    public void updateALMtable(long timestamp) {
        ArrayList almData = iface.getAlertData(timestamp);
        // Scan alertlog results for updates
        for (int i=0;i<almData.size();i++) {
            // AlertMap from DB ArrayList
            Map alertMap = (Map) almData.get(i);
            int alertid = Integer.valueOf(alertMap.get("Alert").toString());
            Map tableMap = new HashMap();
            while(alertsMapUpdating){
                try {                     
                    Thread.sleep(timestamp);
                } catch (InterruptedException ex) {
                }
            }
            if(!alertsMapUpdating){
                int almIDX = alertsMap.get(alertid) == null ? -1 : alertsMap.get(alertid);
                if(almIDX > -1) tableMap = (Map) alertData.get(almIDX);
                if(alertMap.get("Operator").equals("Unassigned")) alertMap.put("Operator", " ");
                if(!alertMap.equals(tableMap)) {
                    updateAlertTableModel(alertMap, almIDX);
                }
            }
        }        
    }
    private void initTabs() {
        for (int i=0;i<mainTab.getTabCount();i++) {
            mainTab.setBackgroundAt(i, colorMap.get("header"));
        }
        UIManager.put("TabbedPane.selected", colorMap.get("menu"));
    }
    public JPopupMenu getAlertTablePopupMenu() {
        return(alertTablePopupMenu);
    }
    
class CustomAlertDataRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null;
    GradientPaint gp = null;    

    
    public CustomAlertDataRenderer() {
        super();
        setOpaque(false);
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));
    }   
    @Override
    public Component getTableCellRendererComponent(
         JTable jt,
         Object value,
         boolean isSelected,
         boolean hasFocus,
         int row,
         int column
         ){
        Component comp = super.getTableCellRendererComponent(jt,value,isSelected,hasFocus,row,column);
        Rectangle rect = jt.getCellRect(row, column, isSelected);
        paintingRect = rect;
        JLabel label = (JLabel)comp;
        label.setFont(fontMap.get("GA11B"));
        setForeground(colorMap.get("black"));
        // Color By Severity
        String severity = jt.getModel().getValueAt(row, 1).toString().trim();
        if (severity.length() < 1) severity = "indeterminate"; 
        int[] sels = jt.getSelectedRows();
        if(isSelected) {
            label.setFont(fontMap.get("GA14B"));
            if(rect != null) {
                if(sels[0] == row || sels.length == 1) {            
                    gp = new GradientPaint(0,0,colorMap.get(severity).darker(),0,rect.height/2,colorMap.get(severity).brighter());
                } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                    gp = new GradientPaint(0,0,colorMap.get(severity).brighter(),0,rect.height/2,colorMap.get(severity).darker());
                } else {
                    gp = new GradientPaint(0,0,colorMap.get(severity).brighter(),0,rect.height/2,colorMap.get(severity).brighter());
                }
            }
        } else if(!isSelected) {
            if(rect != null && severity != null) {
                gp = new GradientPaint(0,0,colorMap.get(severity).brighter(),(rect.width * 7)/8,0,colorMap.get(severity).darker());
            }
        }
        int[] colAlignments = new int[]{0,0,2,2,4,0,0,2,2,0};
            label.setHorizontalAlignment(colAlignments[column]);
            ImageIcon icon = new ImageIcon();
            label.setIcon(icon);
            label.setIconTextGap(6);
        // ToolTip  (Middle MouseWheel Button Activates)
        Color bg = colorMap.get("menu");
        String tooltext =   "<HTML><BODY>"+
                            "<TABLE BORDER CELLPADDING=8 BGCOLOR=\"#"+Integer.toHexString(bg.getRGB()&0x00ffffff)+"\">"+
                            "<TH><H3>"+
                            jt.getModel().getColumnName(column)+
                            "</H3></TH><TD WIDTH=500><P STYLE=\"word-wrap:break-word;width:100%;left:0\"><B>"+
                            value+
                            "</B></P></TD></TABLE></BODY></HTML>";            
        label.setToolTipText(tooltext);
        ToolTipManager.sharedInstance().setInitialDelay(3750);
        ToolTipManager.sharedInstance().setDismissDelay(30000);
        ToolTipManager.sharedInstance().setReshowDelay(1200);   
        return(label);
    }
    public GradientPaint getColumnGradient() {
        return gp;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
        Graphics2D g2 = (Graphics2D)g;
        g2.setPaint(gp);
        g2.fillRect( 0, 0, rect.width, rect.height ); 
        super.paintComponent(g);
    }
 }    
class CustomGatewayDataRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null,lpr = null; 
    GradientPaint gp = null, hoverGradient, columnGradient;   
    public CustomGatewayDataRenderer() {
        super();
        setOpaque(false);
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));        
    }
	@Override
 public Component getTableCellRendererComponent( 
            JTable jt,
		    Object value, 
		    boolean isSelected,
            boolean hasFocus,
		    int row, 
		    int column) {
        int[] sels = jt.getSelectedRows();
        Component comp = super.getTableCellRendererComponent(jt ,value,isSelected,hasFocus,row,column);
        Rectangle rect = jt.getCellRect(row, column, isSelected);
        paintingRect = rect;
        JLabel label = (JLabel)comp;
        label.setFont(fontMap.get("GA11B"));
        setForeground(colorMap.get("black"));
        String gstate = jt.getModel().getValueAt(row, 0).toString().trim();
        if (gstate.length() < 1) {
            gstate = "INACTIVE";
        } 
        if(isSelected) {
            label.setFont(fontMap.get("GA14B"));
            if(sels[0] == row || sels.length == 1) {            
                gp = new GradientPaint(0,0,colorMap.get(gstate).darker(),0,rect.height/2,colorMap.get(gstate).brighter());
            } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                gp = new GradientPaint(0,0,colorMap.get(gstate).brighter(),0,rect.height/2,colorMap.get(gstate).darker());
            } else {
                gp = new GradientPaint(0,0,colorMap.get(gstate).brighter(),0,rect.height/2,colorMap.get(gstate).brighter());
            }
        } else {
            gp = new GradientPaint(0,0,colorMap.get(gstate).brighter(),(rect.width * 7)/8,0,colorMap.get(gstate).darker());
        }
            int[] colAlignments = new int[]{0,2,2,2,0,2,0,0,0,0}; 
            label.setHorizontalAlignment(colAlignments[column]);
            ImageIcon icon = new ImageIcon();
            label.setIcon(icon);
            label.setIconTextGap(6);
        if(jt.getModel().getValueAt(row, 0).toString().trim().equals("ACTIVE")) {
            // If context exists, show up time
            // If Archive ON & Archive exists, show last 10 lines of archive
        }
 	return label;                   
    }
    public void setColumnGradient(GradientPaint gp) {
        this.columnGradient = gp;
    }
    public GradientPaint getColumnGradient() {
        return columnGradient;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
 
        Graphics2D g2 = (Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
 
        super.paintComponent(g);
    }
}
class CustomInhibitDataRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null,lpr = null; 
    GradientPaint gp = null, hoverGradient, columnGradient;    
    public CustomInhibitDataRenderer() {
        super();
        setOpaque(false);
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));        
    }    
    @Override
 public Component getTableCellRendererComponent(
         JTable jt,
         Object value,
         boolean isSelected,
         boolean hasFocus,
         int row,
         int column
         ){
        int[] sels = jt.getSelectedRows();
        Component comp = super.getTableCellRendererComponent(jt ,value,isSelected,hasFocus,row,column);
        Rectangle rect = jt.getCellRect(row, column, isSelected);
        paintingRect = rect;
        JLabel label = (JLabel)comp;
        label.setFont(fontMap.get("GA10B"));
        setForeground(colorMap.get("black"));
        if(isSelected) {
            label.setFont(fontMap.get("GA12B"));
            if(row % 2 == 0) {
                if(sels[0] == row || sels.length == 1) {            
                    gp = new GradientPaint(0,0,colorMap.get("inhTablePri").darker(),0,rect.height/2,colorMap.get("inhTablePri").brighter());
                } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                    gp = new GradientPaint(0,0,colorMap.get("inhTablePri").brighter(),0,rect.height/2,colorMap.get("inhTablePri").darker());
                } else {
                    gp = new GradientPaint(0,0,colorMap.get("inhTablePri").brighter(),0,rect.height/2,colorMap.get("inhTablePri").brighter());
                }
            } else {
                if(sels[0] == row || sels.length == 1) {            
                    gp = new GradientPaint(0,0,colorMap.get("inhTableAlt").darker(),0,rect.height/2,colorMap.get("inhTableAlt").brighter());
                } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                    gp = new GradientPaint(0,0,colorMap.get("inhTableAlt").brighter(),0,rect.height/2,colorMap.get("inhTableAlt").darker());
                } else {
                    gp = new GradientPaint(0,0,colorMap.get("inhTableAlt").brighter(),0,rect.height/2,colorMap.get("inhTableAlt").brighter());
                }
            }
        } else {
            if(row % 2 == 0) {
                gp = new GradientPaint(0,0,colorMap.get("inhTablePri").brighter(),(rect.width * 7)/8,0,colorMap.get("inhTablePri").darker());
            } else {
                gp = new GradientPaint(0,0,colorMap.get("inhTableAlt").brighter(),(rect.width * 7)/8,0,colorMap.get("inhTableAlt").darker());
            }
        }
        if(column < 4|| column == 5) {
            label.setHorizontalAlignment(JLabel.LEFT);
            ImageIcon icon = new ImageIcon();
            label.setIcon(icon);
            label.setIconTextGap(6);
        } else {
            label.setHorizontalAlignment(JLabel.CENTER);
        }
 	return label;        
    }
    public void setColumnGradient(GradientPaint gp) {
        this.columnGradient = gp;
    }
    public GradientPaint getColumnGradient() {
        return columnGradient;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
 
        Graphics2D g2 = (Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
 
        super.paintComponent(g);
    }
}
class CustomHeaderRenderer extends DefaultTableCellRenderer {
    Rectangle paintingRect = null,lpr = null; 
    JTableHeader header = null;
    GradientPaint gp = null, hoverGradient, columnGradient;
 
    public CustomHeaderRenderer() {
        super();
        setOpaque(false);
        setFont(fontMap.get("GA16B"));
        BevelBorder border = (BevelBorder) BorderFactory.createBevelBorder(
                BevelBorder.RAISED, 
                colorMap.get("menu"), 
                colorMap.get("menu").brighter(), 
                colorMap.get("header"), 
                colorMap.get("header").darker());                     
        setBorder((border));                      
    }   
    public void setColumnGradient(GradientPaint gp) {
        this.columnGradient = gp;
    }
    public GradientPaint getColumnGradient() {
        return columnGradient;
    }
    @Override
    public void paintComponent(Graphics g) {
        Rectangle rect = paintingRect;
 
        Graphics2D g2 = (Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
 
        super.paintComponent(g);
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int col) {
        int[] colAlignments = null;
        if(table.getName().equals("Alert")) {
            colAlignments = new int[]{0,0,2,2,4,0,0,2,2,0};
        } else if(table.getName().equals("Gateway")) {
            colAlignments = new int[]{0,2,2,2,0,2,0,0,0,0};
        } else if(table.getName().equals("Inhibit")) {
            colAlignments = new int[]{0,0,0,0};
        }
        if(colAlignments != null) setHorizontalAlignment(colAlignments[col]);
        Rectangle rect = table.getTableHeader().getHeaderRect(col);
        gp = new GradientPaint(0, 0, colorMap.get("header").brighter(), 0, rect.height/2, colorMap.get("header"));
        setForeground(colorMap.get("black")); 
        paintingRect = rect;
        setText( value == null ? "" : value.toString() );
        ImageIcon icon = new ImageIcon();
        setIcon(icon);
        setIconTextGap(6);
        return(this);
    }
    }
public class TableMap extends AbstractTableModel implements TableModelListener {
  protected TableModel model;
  public TableModel getModel() {
    return model;
  }
  public void setModel(TableModel model) {
    this.model = model;
    model.addTableModelListener(this);
  }
  // By default, implement TableModel by forwarding all messages
  // to the model.
  public Object getValueAt(int aRow, int aColumn) {
    return model.getValueAt(aRow, aColumn);
  }
  
        @Override
  public void setValueAt(Object aValue, int aRow, int aColumn) {
    model.setValueAt(aValue, aRow, aColumn);
  }
  
  public int getRowCount() {
    return (model == null) ? 0 : model.getRowCount();
  }
  
  public int getColumnCount() {
    return (model == null) ? 0 : model.getColumnCount();
  }
  
        @Override
  public String getColumnName(int aColumn) {
    return model.getColumnName(aColumn);
  }
  
        @Override
  public Class getColumnClass(int aColumn) {
    return model.getColumnClass(aColumn);
  }
  
        @Override
  public boolean isCellEditable(int row, int column) {
     return model.isCellEditable(row, column);
  }
//
// Implementation of the TableModelListener interface,
//
  // By default forward all events to all the listeners.
  public void tableChanged(TableModelEvent e) {
    fireTableChanged(e);
  }
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable alertTable;
    private javax.swing.JPopupMenu alertTablePopupMenu;
    private javax.swing.JScrollPane alertdisplayScrollPane;
    private javax.swing.JTable gateTable;
    private javax.swing.JPopupMenu gateTablePopupMenu;
    private javax.swing.JScrollPane gateTableScrollPane;
    private javax.swing.JScrollPane gatewayScrollPane;
    private javax.swing.JButton jButton1;
    private javax.swing.JList jList1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTabbedPane logsTab;
    private javax.swing.JTabbedPane mainTab;
    private javax.swing.JMenu menuEditors;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuIDEAS;
    private javax.swing.JMenu menuIDEASDump;
    private javax.swing.JMenu menuIDEASReInit;
    private javax.swing.JMenu menuIDEASStats;
    private javax.swing.JMenu menuIDEASTrace;
    private javax.swing.JCheckBoxMenuItem menuItemALERTSclear_CheckBox;
    private javax.swing.JMenuItem menuItemIDEASDumpEventSummary;
    private javax.swing.JMenuItem menuItemIDEASDumpEvents;
    private javax.swing.JMenuItem menuItemIDEASReInitALL;
    private javax.swing.JMenuItem menuItemIDEASReInitLOCK;
    private javax.swing.JMenuItem menuItemIDEASReInitMib;
    private javax.swing.JMenuItem menuItemIDEASReInitRules;
    private javax.swing.JMenuItem menuItemIDEASReInitRulesOnly;
    private javax.swing.JMenuItem menuItemIDEASReInitUNLOCK;
    private javax.swing.JMenuItem menuItemIDEASTrace;
    private javax.swing.JMenuItem menuItemStatsALERTDEF;
    private javax.swing.JMenuItem menuItemStatsALL;
    private javax.swing.JMenuItem menuItemStatsATTR;
    private javax.swing.JMenuItem menuItemStatsCACHELST;
    private javax.swing.JMenuItem menuItemStatsCLASS;
    private javax.swing.JMenuItem menuItemStatsCLASSATTR;
    private javax.swing.JMenuItem menuItemStatsEVENT;
    private javax.swing.JMenuItem menuItemStatsEVTSUM;
    private javax.swing.JMenuItem menuItemStatsMEMSUM;
    private javax.swing.JMenuItem menuItemStatsMEMTAB;
    private javax.swing.JMenuItem menuItemStatsMO;
    private javax.swing.JMenuItem menuItemStatsMOATTR;
    private javax.swing.JMenuItem menuItemStatsMOREPAS;
    private javax.swing.JMenuItem menuItemStatsRULE;
    private javax.swing.JMenuItem menuItemStatsRULESTATS;
    private javax.swing.JMenuItem menuItem_AlertEditor;
    private javax.swing.JMenuItem menuItem_AuthEditor;
    private javax.swing.JMenuItem menuItem_DialogEditor;
    private javax.swing.JMenuItem menuItem_Exit;
    private javax.swing.JMenuItem menuItem_GatewayEditor;
    private javax.swing.JMenuItem menuItem_ObjectEditor;
    private javax.swing.JMenuItem menuItem_RuleEditor;
    private javax.swing.JMenu menuOptions;
    private javax.swing.JMenu menuOptionsALERTS;
    private javax.swing.JTextField notifierInput;
    private javax.swing.JPanel notifierTab;
    private javax.swing.JTabbedPane pautilTab;
    private javax.swing.JTabbedPane processesTab;
    private javax.swing.JTabbedPane ruleviewerTab;
    private javax.swing.JTabbedPane statisticsTab;
    private javax.swing.JTabbedPane svnTab;
    private javax.swing.JTabbedPane tracesTab;
    // End of variables declaration//GEN-END:variables
}
