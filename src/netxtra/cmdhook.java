package netxtra;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistence.antlr.collections.impl.Vector;

public class cmdhook {
    Vector<String> resultVector = new Vector<String>();
    Process ipmhhook;
    Thread inThread;
    Thread errThread;
    Thread ipmhProbeThread;
    Thread ipmhMessageThread;
    InputStream inStream;
    InputStream errStream;
    String sendmsg;
    
    public void startProcess(String cmd) throws IOException {
        ipmhhook = Runtime.getRuntime().exec(cmd);
        errStream = ipmhhook.getErrorStream();
        errThread = streamThread(errStream);
        errThread.setName("Errors");
        inStream = ipmhhook.getInputStream();
        inThread = streamThread(inStream);
        inThread.setName("Input");
        ipmhProbeThread = probeThread();
        ipmhMessageThread = messageThread();
        errThread.start();
        inThread.start();
    //	ipmhProbeThread.start();
        ipmhMessageThread.start();
    }

    public void sendMessage(String msg) throws IOException {
	sendmsg = msg;
	BufferedOutputStream outStream = new BufferedOutputStream(ipmhhook.getOutputStream());
	BufferedWriter output = new BufferedWriter(new OutputStreamWriter(outStream));
	outStream.write(sendmsg.getBytes());
	outStream.write(System.getProperty("line.separator").getBytes());
	output.flush();
    }
    
    private Thread probeThread(){
        Thread thread = new Thread("ProbeThread") {
	    @Override
        @SuppressWarnings("static-access")
	    public void run() {
		while(true) {
		    try {
			Thread.currentThread().sleep(1000);
			sendMessage("new");
		    } catch (IOException ex) {
			Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
		    } catch (InterruptedException ex) {
			Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
		    }
		}
	    }
	};
	return(thread);
    }
    
    private Thread messageThread() {
        Thread thread = new Thread("MessageThread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                while(true) {
                    try {
                        if (resultVector != null & resultVector.size() > 0) {
                            int marker = resultVector.size();
                            for (int idx = 0; idx < marker; idx++) {
                                System.out.println(resultVector.get(0));
                                resultVector.remove(0);
                            }
                        }
                        Thread.currentThread().sleep(250);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        return(thread);
    }
    
    private Thread streamThread(final InputStream stream){
        Thread thread = new Thread("StreamThread") {
        @Override
	    public void run() {
		try {
		    while(true){
                BufferedReader streamRead = new BufferedReader(new InputStreamReader(stream));
                boolean reading = streamRead.ready();
                while(reading) {
                        String line = streamRead.readLine();
                        if(line != null & line.length() > 0) {
                            storeResults(line);
System.out.println(line);
                        } else {
                            reading = false;
                        }
                }
			            Thread.sleep(250);
		    }
		} catch (InterruptedException ex) {
		    Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
		    Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	};
        return(thread);
    }
    
    public void storeResults(String value) {
        if (value != null & !value.equals(sendmsg)) {
	    resultVector.add(value);
	}
    }
    
}
