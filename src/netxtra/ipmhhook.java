package netxtra;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import jTELNET.TelnetWrapper;
import java.util.ArrayList;
import java.util.HashMap;

public class ipmhhook {
    private Pkg_Interface iface;
    private Map envMap;
    public Vector<String> resultVector = new Vector<String>();  
    public boolean ready = false;
    public TelnetWrapper telnet;
    public Socket ipmhhook;
    private int newMessageID = 0;
    private int oldMessageID = 0;
    private int netExAdminPID;
    private boolean stx = false;
    private int msgID = 0;
    private int msgCID = 0;
    private int msgSID= 0;
    private String msgText = "";
    public Thread inThread;
    public InputStream inStream;
    private String user = null;
    private String pass = null;
    private String sendmsg;
    ArrayList<Map> gatewayData = new ArrayList<Map>();
    public ipmhhook(Pkg_Interface face) {
        iface = face;
        envMap = iface.getEnvironment();
        try {
            user = "ag991n";
            pass = "@d3lph1a";
            String syshost = "nmsdpn1";
            String sysusr = "netx";
            String syspss = "net123!";
            // Login to server
            telnet = new TelnetWrapper();
            telnet.connect("snt1", 23);
            telnet.login(user, pass);
            telnet.waitfor("snt1%");
            telnet.send("ssh -l "+sysusr+" "+syshost);
            telnet.waitfor("password");
            telnet.send(syspss);
            telnet.waitfor("Select:");
            telnet.send("3");
            // Logged in .. Start ipmhTool
            telnet.send("ipmhTool -s "+envMap.get("OSI_SYSTEM")+" -d "+envMap.get("OSI_DBPTR")+" -n");
            telnet.waitfor("ipmhTool");
            telnet.send("drop 910");
            telnet.waitfor("ipmhTool");
            telnet.send("subscribe 518");
            telnet.waitfor("ipmhTool");
            telnet.send("publish 500 !93^"+envMap.get("OSI_IDEAS_HOST")+"^"+netExAdminPID+"^0@");
            telnet.waitfor("ipmhTool");
            telnet.send("retain 1000");
            telnet.waitfor("ipmhTool");
            telnet.send("alias PRE "+envMap.get("OSI_SYSTEM")+".netexpert.*");
            telnet.waitfor("ipmhTool");                
            // Request netExAdmin registered Gateways
            inStream = telnet.in;
            inThread = streamThread(telnet.socket);
            inThread.setName("ipmhInput");
            inThread.start();
            Thread alertThread = alertPollThread();
            alertThread.start();
            messageThread().start();
            probeThread().start();
//            int[] ipmhClasses = new int[]{86,145,146,149,151,333,500,501,503,506,508,511,516,518,520,550,565,570,574,575,596,598,599,600,720,730,740,910,1307,1310,1327,1339,1329,1331,1334,1335,1341,1344,1337,1328,1318};
            int[] ipmhClasses = new int[]{500,501,503,506,508,511,516,518,520};
            for (int classkey : ipmhClasses) {
                telnet.send("subscribe "+classkey);
                telnet.waitfor("ipmhTool");
            }
            ready = true;
        } catch (IOException ex) {
            Logger.getLogger(ipmhhook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    protected Thread messageThread() {
        Thread thread = new Thread("MessageThread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                while(true) {
                    try {
                        if (resultVector != null) {
                            for (int idx=0; idx<resultVector.size(); idx++) {
                                processMessage(resultVector.get(0));
                                resultVector.remove(0);
                            }
                        }
                        Thread.currentThread().sleep(250);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.WARNING, null, ex);
                    }
                }
            }
        };
        return(thread);
    }
    protected Thread probeThread(){
        Thread thread = new Thread("ProbeThread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                while(true) {
                    try {
                    Thread.currentThread().sleep(1000);
                    sendMessage("new");
                    } catch (InterruptedException ex) {
                    Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        return(thread);
    }
    protected void processMessage(String msg) {
        // Grab Class and MessageID
        if (msg.contains("class(")) {
            String[] cidt1 = msg.split("\\)");
            String[] cidt2 = cidt1[0].split("class\\(");
            msgCID = Integer.valueOf(cidt2[1]);
            String[] midt1 = msg.split(" ");
            msgID = Integer.valueOf(midt1[0]);
            msgSID = 0;
            stx = false;
            msgText = "";
        }         
        // Grab Class perameter
        if (msg.contains("(<STX>")) {
            String[] sidt0 = msg.split("<STX>");
            String[] sidt1 = sidt0[1].split("<");
            if(!sidt1[0].toString().contains("[A-Za-z]")) msgSID = Integer.valueOf(sidt1[0].trim());
        } 
        String[] msgContents = msg.split(" ");
        for (String content:msgContents) {
            if (content.contains("text(") & content.endsWith(")")) {
                msgText = content.split("text\\(")[1].replace(")", "");
                stx = false;
            } else if (content.contains("text(") & !content.endsWith(")"))  {
                msgText = content.split("text\\(")[1];
                stx = true;
            } else if (content.endsWith(")")) {
                msgText += content.replace(")", "");
                stx = false;
            } else {
                if (stx) {
                    msgText += content;
                }
            }
        }
        if(!stx) {
System.out.println(msgID+" "+msgCID+":"+msgSID+" "+msgText);           
            switch(msgCID) {
            case 500:
                break;
            case 501:
                switch(msgSID) {
                case 11:
                    procMSG_501_11(msgText);
                    break;
                case 12:
                    procMSG_501_12(msgText);
                    break;
                case 21:
                    procMSG_501_21(msgText);
                    break;
                case 28:
                    procMSG_501_28(msgText);
                    break;
                case 29:
                    procMSG_501_29(msgText);
                    break;
                }
            case 511:
                switch(msgSID) {
                case 8:
                    procMSG_511_8(msgText);
                    break;
                case 29:
                    procMSG_511_29(msgText);
                    break;
                }            
            case 516:
                break;
            case 518:
                switch(msgSID) {
                case 1:
                    procMSG_518_1(msgText);
                    break;
                case 2:
                    procMSG_518_2(msgText);
                    break;
                }
            case 596:
                break;
            case 910:
                break;
            }
        }
    }    
    protected Thread streamThread(final Socket conn){
        Thread thread = new Thread("StreamThread") {
            @Override
            public void run() {
            try {
                while(true){
                    InputStreamReader input = new InputStreamReader(conn.getInputStream());
                    boolean reading = conn.isConnected();
                    while(reading) {
                        boolean storage = true;
                        String prompt = "";
                        String data = "";
                        boolean feeding = input.ready();
                        while(feeding) {
                            storage = true;
                            int c = input.read();
                            if(c ==62) prompt += (char)c;
                            if(c == 13) feeding = false;
                            if(c > 31 & c < 127) {
                                data += ((char)c);
                            }
                            if(prompt.equals(">>>")) {
                                feeding = false;
                            }
                        }
                        if(data != null & data.length() > 0) {
                            data = data.trim();
                            if (data.equals(sendmsg)) {
                                storage = false;
                            } else if (data.startsWith(envMap.get("OSI_SYSTEM")+".") & data.contains("ipmhTool:")) {
                                storage = false;
                            } else if (data.startsWith("publish") & data.endsWith("@")) {
                                storage = false;
                            } else if (data.startsWith("retained num") & data.contains("serial num")) {
                                storage = false;
                            } else if (data.contains("Newest Message is number ")) {
                                newMessageID = Integer.valueOf(data.split("Newest Message is number ")[1]);
                                if (newMessageID != oldMessageID) {
                                    sendMessage("text "+(oldMessageID + 1)+" "+newMessageID);
                                    oldMessageID = newMessageID;
                                }
                                storage = false;
                            }
                            if (storage) storeResults(data);
                        }
                    }
                }
            } catch (IOException ex) {
//                Logger.getLogger(cmdhook.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
        };
        return(thread);
    }
    protected Thread alertPollThread() {
        Thread thread = new Thread("AlertPollThread") {
            @Override
            public void run() {
                try {
                    iface.getGUI().enableAlertTable(true);
                    while (!iface.getOraDB().dbConn.isClosed()) {
                        if (iface.isNewAlerts()) {
                            iface.getGUI().updateALMtable(iface.getLAST1());
                        }
                        Thread.sleep(iface.getGUI().alertUpdateDelay * iface.genRandom(7000));
                    }
                }   catch (InterruptedException ex) {
                        iface.getGUI().enableAlertTable(false);
                } catch (SQLException ex) {
                        iface.getGUI().enableAlertTable(false);
                }
            }
        };
        return(thread);
    }
    public void sendMessage(String text) {
        sendmsg = text;
        if(envMap.get("OSNAME").equals("SunOS")) {
            
        } else if(envMap.get("OSNAME").toString().contains("Windows")) {
            try {
                telnet.send(text);
            } catch (IOException ex) {
                Logger.getLogger(ipmhhook.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void sleep(int msec) {
            try {
                Thread.sleep(msec);
            } catch (InterruptedException ex) {
                Logger.getLogger(ipmhhook.class.getName()).log(Level.SEVERE, null, ex);
            }        
    }
    public void storeResults(String value) {
        if (value != null) {
            resultVector.add(value);
        }
    }
    protected void procMSG_501_11(String msg) {
        iface.getGUI().updateALMtable();
    }
    protected void procMSG_501_12(String msg) {
        String[] almoperPair = msg.split("<FS>");
        for(String almDelimitOp:almoperPair) {
            if (almDelimitOp.contains("<STX>")) continue;
            int alertid;
            if(almDelimitOp.contains("<GS>")) {
                alertid = Integer.valueOf(almDelimitOp.split("<GS>")[0]);
            } else {
                alertid = Integer.valueOf(almoperPair[1].replace("<ETX>", ""));
            }
            iface.getGUI().removeALMtableItem(alertid);
        }
    }
    protected void procMSG_501_21(String msg) {
        if(iface.getGUI().isAlertTableHighLighted() || iface.getGUI().isAlertTablePopupMenuActive()) return;
        int alertid = Integer.valueOf(msg.split("<FS>")[1]);
        iface.getGUI().updateALMtable(iface.getLAST1());
    }
    protected void procMSG_501_28(String msg) {
        iface.getGUI().updateALMtable();
    }
    protected void procMSG_501_29(String msg) {
        iface.getGUI().updateALMtable();
    }    
    protected void procMSG_511_8(String msg) {
        if(msg.contains("inhibit")) {
            iface.getGUI().createAlertTableModel(iface.getGUI().alertData);
            iface.getGUI().createALMTablePopupMenu();
        }
    }
    protected void procMSG_511_29(String msg) {
        iface.getGUI().updateALMtable();
    } 
    protected void procMSG_518_1(String msg) {
        gatewayData.clear();
        String[] gateList = msg.split("<FS>");
        int i =0;
        for (String ldata:gateList) {
            if(ldata.contains("<GS>")) {
                Map dataMap = new HashMap();
                String[] gateData = ldata.split("<GS>");
                String[] mapNames = {"ClassName","ManagerName","GatewayName","HostName","PortName","Dialog","CutThrough","Archive","Status","MgrPortKey"};
                int labelkey = 0;
                for (String cdata:gateData) {
                    String value = cdata.replace("<ETX>", "");
                    String Name = mapNames[labelkey];
                    dataMap.put(Name, value);      
                    labelkey++;
                }
                gatewayData.add(i, dataMap);
                i++;
            }
        }
        iface.updateGWtable(gatewayData);
    }
    protected void procMSG_518_2(String msg) {
        String[] gateList = msg.split("<FS>");
        String[] updateData = gateList[1].replace("<ETX>", "").split("<GS>");
        String[] mapNames = {"ClassName","ManagerName","GatewayName","HostName","PortName","Dialog","CutThrough","Archive","Status","MgrPortKey"};
        Map upgateMap = new HashMap();
        for (int i=0;i < mapNames.length;i++) {
            upgateMap.put(mapNames[i],updateData[i]);
        }
        boolean update = false;
        for (int i=0;i < gatewayData.size();i++) {
           Map gateMap = gatewayData.get(i);   
           if (gateMap.get("MgrPortKey").equals(updateData[9])) {
               for (int a=0;a < gateMap.size();a++) {
                   if (!gateMap.get(mapNames[a]).equals(upgateMap.get(mapNames[a]))) {
                       update = true;
                   }
               }
               if(update) {
                   gatewayData.set(i, upgateMap);
                   iface.updateGWtable(gatewayData);
                   update = false;
                   break;
               }
           }
        }
    }
}
