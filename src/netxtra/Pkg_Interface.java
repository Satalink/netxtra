/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package netxtra;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author ag991n
 */
public interface Pkg_Interface {
    public void exitApp();
    public int genRandom(int x);
    public Map getEnvironment();
    public void print(String text);
    public void setEnvironment();
    public ipmhhook getHook();
    public OraDB getOraDB();
    public GUI getGUI();
    public String getOSName();
    public URL getURL(String link);
    public boolean isNewAlerts();
    public ArrayList getAlertData();
    public ArrayList getAlertData(int alertid);
    public ArrayList getAlertData(long timestamp);
    public long getLAST1();
    public void setLAST1(long ts);
    public void updateGWtable(ArrayList gwdata);
}
