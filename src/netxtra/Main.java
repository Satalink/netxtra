package netxtra;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Neal Garrett
 */
public class Main implements Pkg_Interface {
    public ipmhhook ihook;    
    public cmdhook chook;
    public OraDB db;
    public GUI eGUI;
    public Random random = new Random();
    public long last1 = 0;
    Map<String, String> envMap = new HashMap<String, String>();
    String PROGNAME = "netXtra";
    public Main() {
        try {
            last1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("1970-01-01 00:00:00").getTime();
            setEnvironment();
            db = new OraDB(this);
            eGUI = new GUI(this);
            eGUI.setTitle(PROGNAME + " (" + envMap.get("OSI_SYSTEM") + ")");
            String refFile = "icon_toolbox.gif";
            Image winicon = new ImageIcon(getURL(refFile)).getImage();
            eGUI.setIconImage(winicon);
            ArrayList gwData = getGatewayData();
            eGUI.createGWtableModel(gwData);
            ArrayList almData = getAlertData();
            eGUI.createAlertTableModel(almData);
//Place Splash Here
// Remove above setVisble after Splash code inserted
            if (getOSName().contains("Windows")) {
                ihook = new ipmhhook(this);
            } else if (getOSName().contains("Sun")) {
                chook = new cmdhook();
            }
            eGUI.setVisible(true);
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            Main eMain = new Main();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// Main Methods    
    public void exitApp() {
        try {
            eGUI.setVisible(false);
            if(getOSName().contains("Windows")) {
                db.DbClose(db.dbConn);
                if(ihook.ready) ihook.sendMessage("quit");
                ihook.telnet.disconnect();
            }
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int genRandom(int x){
        return(random.nextInt(x));
    }
    public String[] getContextList() {
        String[] context = null;
        return(context);
    }
    public ArrayList getGatewayData() {
        ArrayList gatewayData = null;
        String sql = 
                "select "+
                "mgrconf.mgrport as \"GatewayName\", "+
                "class.name as \"ClassName\", "+
                "mo.name as \"ManagerName\", "+
                "mgrconf.host as \"HostName\", "+
                "mgrconf.port as \"PortName\" "+
                "from mgrconf "+
                "left join mo on mo.mo = mgrconf.manager "+
                "left join class on class.class = mo.class "+
                "where mgrconf.hardwired > 1 "+
                "order by class.name";
        gatewayData = db.DbSelect(sql);
        return(gatewayData);
    }
    public ArrayList getAlertData() {
        ArrayList alertData = null;
        String sql =
                "select "+
                "alertlog.alert as \"Alert\", "+
                "alertlog.times as \"Count\", "+
                "netxoper.name as \"Operator\", "+
                "codepoint.description as \"Severity\", "+
                "alertlog.text1 as \"Description\", "+
                "to_char (alertlog.first1, 'YYYY-MM-DD  HH24:MI:SS') as \"CreateTime\", "+
                "to_char (alertlog.last1, 'YYYY-MM-DD  HH24:MI:SS') as \"UpdateTime\", "+
                "alertid.name as \"AlertName\", "+
                "mo.name as \"AMO\", "+
                "class.name as \"Class\" "+
                "from alertlog "+
                "left join codepoint on codepoint.codepoint = alertlog.severity "+
                "left join mo on mo.mo = alertlog.mo "+
                "left join class on class.class = mo.class "+
                "left join alertid on alertid.alertid = alertlog.alertid "+
                "left join netxoper on netxoper.netxoper = alertlog.ackoper "+
                "where alertlog.status != 2 "+
                "and codepoint.category = 102 "+
                "order by alertlog.last1 desc";
        alertData = db.DbSelect(sql);
        return(alertData);
    }
    public ArrayList getAlertData(int alertid){
        ArrayList alertData = null;
        String sql =
                "select "+
                "alertlog.alert as \"Alert\", "+
                "alertlog.times as \"Count\", "+
                "netxoper.name as \"Operator\", "+
                "codepoint.description as \"Severity\", "+
                "alertlog.text1 as \"Description\", "+
                "to_char (alertlog.first1, 'YYYY-MM-DD  HH24:MI:SS') as \"CreateTime\", "+
                "to_char (alertlog.last1, 'YYYY-MM-DD  HH24:MI:SS') as \"UpdateTime\", "+
                "alertid.name as \"AlertName\", "+
                "mo.name as \"AMO\", "+
                "class.name as \"Class\" "+
                "from alertlog "+
                "left join codepoint on codepoint.codepoint = alertlog.severity "+
                "left join mo on mo.mo = alertlog.mo "+
                "left join class on class.class = mo.class "+
                "left join alertid on alertid.alertid = alertlog.alertid "+
                "left join netxoper on netxoper.netxoper = alertlog.ackoper "+
                "where alertlog.status != 2 "+
                "and codepoint.category = 102 "+
                "and alertlog.alert = "+alertid+" "+
                "order by alertlog.last1 desc";
        alertData = db.DbSelect(sql);
        return(alertData);        
    }
    public ArrayList getAlertData(long timestamp) {
        ArrayList alertData = null;
        String timedate = new java.sql.Timestamp(timestamp).toString().split("\\.")[0];
        String sql =
                "select "+
                "alertlog.alert as \"Alert\", "+
                "alertlog.times as \"Count\", "+
                "netxoper.name as \"Operator\", "+
                "codepoint.description as \"Severity\", "+
                "alertlog.text1 as \"Description\", "+
                "to_char (alertlog.first1, 'YYYY-MM-DD HH24:MI:SS') as \"CreateTime\", "+
                "to_char (alertlog.last1, 'YYYY-MM-DD HH24:MI:SS') as \"UpdateTime\", "+
                "alertid.name as \"AlertName\", "+
                "mo.name as \"AMO\", "+
                "class.name as \"Class\" "+
                "from alertlog "+
                "left join codepoint on codepoint.codepoint = alertlog.severity "+
                "left join mo on mo.mo = alertlog.mo "+
                "left join class on class.class = mo.class "+
                "left join alertid on alertid.alertid = alertlog.alertid "+
                "left join netxoper on netxoper.netxoper = alertlog.ackoper "+
                "where alertlog.status != 2 "+
                "and codepoint.category = 102 "+
                "and last1 >= to_date('"+(timedate)+"', 'YYYY-MM-DD HH24:MI:SS') "+
                "order by alertlog.last1 desc";
        alertData = db.DbSelect(sql);
        return(alertData);        
    }
    public boolean isNewAlerts() {
        boolean update = false;
        if(eGUI.isAlertTablePopupMenuActive()||eGUI.isAlertTableHighLighted()) return(false);
        try {
            String sql = "select " + "to_char(max(last1), 'YYYY-MM-DD HH24:MI:SS') as \"TIMESTAMP\"" + "from alertlog";
            Map alertMap = (Map) db.DbSelect(sql).get(0);
            long timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(alertMap.get("TIMESTAMP").toString()).getTime()/1000;
            if ((timestamp) > last1) {
                update = true;
                last1 = timestamp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return update;
    }
    public Map getEnvironment() {
        return(envMap);
    }
    public void setEnvironment() {
            envMap.put("OSNAME", getOSName());
        if(getOSName().contains("SunOS")) {
            envMap.put("OSI_SYSTEM", System.getenv("OSI_SYSTEM"));
            envMap.put("NMS_SYSTEM", "nms"+System.getenv("NMS_GROUP_NUMBER")+"-"+System.getenv("OSI_NMS_NUMBER"));
            envMap.put("OSI_IDEAS_HOST", System.getenv("OSI_IDEAS_HOST"));
            envMap.put("dbDriver", "oracle.jdbc.driver.OracleDriver");
            envMap.put("dbURL", "jdbc:oracle:thin:@"+System.getenv("OSI_DB_SERVER")+":"+System.getenv("OSI_DB_PORT")+":"+System.getenv("ORACLE_SID"));
            envMap.put("dbUser", System.getenv("OSI_DB_USER"));
            envMap.put("dbPass", System.getenv("OSI_DB_PASSWORD"));
            //TODO Get netExAdminPID from file /tmp/(OSI_SYSTEM).netexpert.netExAdmin.context
            
        } else {
            envMap.put("OSI_SYSTEM", "R23");
            envMap.put("NMS_SYSTEM", "nms1-1");
            envMap.put("OSI_IDEAS_HOST", "nmsdpn1");
            envMap.put("dbDriver", "oracle.jdbc.driver.OracleDriver");
            envMap.put("dbURL", "jdbc:oracle:thin:@"+envMap.get("OSI_IDEAS_HOST")+":1521:nms1");
            envMap.put("dbUser", "R23");
            envMap.put("dbPass", "R23");
        }
    }
    public ipmhhook getHook () {
        return(ihook);
    }
    public OraDB getOraDB() {
        return(db);
    }
    public GUI getGUI() {
        return(eGUI);
    }
    public String getOSName() {
    	String osName = System.getProperty("os.name");
        return(osName);
    }       
    public URL getURL(String link) {
        String UniRefLink = null;
        JarFile mainjar = null;
        URL refURL = null;
        try {
            mainjar = new JarFile(PROGNAME+".jar");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Enumeration entries = mainjar.entries();
        while (entries.hasMoreElements()) {
            String curURL = entries.nextElement().toString();
            if(curURL.contains(link)) {
            UniRefLink = curURL;
            break;
            }
        }
            try {
                String curDir = System.getProperty("user.dir");
                refURL = new URL("jar:file:/"+curDir+File.separatorChar+PROGNAME+".jar!/"+UniRefLink);
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            }
        return(refURL);
    }
    public void print(String text){
        System.out.println(text);
    }
    public void putEnvironment(String varName, String value) {
        envMap.put(varName, value);
    }
    public void remEnvironment(String varName) {
        envMap.remove(varName);
    }
    public long getLAST1() {
        return(last1);
    }
    public void setLAST1(long ts) {
        last1 = ts;
    }
    public void updateGWtable(ArrayList gwdata) {
        eGUI.createGWtableModel(gwdata);
    }
}

