
package netxtra;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ag991n
 */
public class OraDB {
    private Pkg_Interface iface;
    private Map envMap;
    public Connection dbConn;
    public OraDB(Pkg_Interface face) {
        iface=face;	
        envMap = iface.getEnvironment();
    }    
    

    public Connection DbClose(Connection dbConn){
        try {                        
            if (dbConn != null && !dbConn.isClosed()) {
                dbConn.close();
                dbConn = null;
            }
            else 
                dbConn = null;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return dbConn;
    }    
    public Connection DbConnect(){
        String dbDriver = envMap.get("dbDriver").toString();
        String dbURL = envMap.get("dbURL").toString();
        String dbUser = envMap.get("dbUser").toString();
        String dbPassWord = envMap.get("dbPass").toString();
        dbConn = null;
        try {
            Class.forName(dbDriver).newInstance();
            dbConn = DriverManager.getConnection(dbURL, dbUser, dbPassWord);
            return dbConn;
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } 
        catch (SQLException e) {
            e.printStackTrace();
        } 
        catch (InstantiationException e) {
            e.printStackTrace();
        } 
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }  
        return(dbConn);
    }
    @SuppressWarnings("unchecked")
    public ArrayList DbSelect(String sqlstatement) {
        ArrayList<Map> resultList = new ArrayList<Map>();
        try {
            if(dbConn == null) {
                dbConn = DbConnect();
            } else if( dbConn.isClosed()) {
                dbConn = DbConnect();
            }
            if(!dbConn.isClosed()) {
                Statement stmt = dbConn.createStatement();
                ResultSet resultset = stmt.executeQuery(sqlstatement);
                ResultSetMetaData rsmd = resultset.getMetaData();
                int columns = rsmd.getColumnCount();
                int i = 0;
                while (resultset.next()) {
                    int col = 1;
                    Map resultMap = new HashMap();
                    while (col <= columns) {
                        String value = resultset.getString(col);
                        String colName = rsmd.getColumnName(col);
                        resultMap.put(colName, value);
                        col++;
                    }
                    resultList.add(i, resultMap);
                    i++;
                }
                resultset.close();
                stmt.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(OraDB.class.getName()).log(Level.SEVERE, sqlstatement, ex);
        }
        return resultList;
    }
    public boolean DbStatement(String sqlstatement) {
        try {
            return(dbConn.createStatement().execute(sqlstatement));
        } catch (SQLException ex) {
            return(false);
        }
    }
}
